# -*- coding: utf-8 -*
from apps.getimage import controller

print ('...start routing module getimage')
from flask import Blueprint

routes = Blueprint('/getimage',__name__)
routes.add_url_rule(
    '',
    'getimage',
    controller.getimagebase64,
    methods = ['GET']
)

print ('...finish routing module getimage')