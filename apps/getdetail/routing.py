# -*- coding: utf-8 -*
from apps.getdetail import controller

print ('...start routing module getdetail')
from flask import Blueprint

routes = Blueprint('/getdetail',__name__)
routes.add_url_rule(
    '',
    'getimage',
    controller.getdetail,
    methods = ['GET']
)
routes.add_url_rule(
    '',
    'updatedetail',
    controller.updatedetail,
    methods = ['POST']
)


print ('...finish routing module getdetail')