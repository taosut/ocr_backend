# -*- coding: utf-8 -*
from flask import request ,send_file
import os
from apps.app import app ,UPLOAD_FOLDER ,ALLOWED_EXTENSIONS,UPLOAD_FOLDER_PNG,UPLOAD_FOLDER_PDF
from ..helper.helper import *
from werkzeug.utils import secure_filename
import datetime
from PIL import Image
from ..s3.s3 import *
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
from ..database import *

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def upload_file():
    now = datetime.datetime.now()
    now = now.strftime("%d-%m-%Y_%H-%M")

    list_png = []
    if request.method == 'POST':
        file = request.files['file']

        if 'file' not in request.files:
            print("in not file")
            return json_response({"Message":"No file selected for uploading"},400)
        elif file.filename == '':
            return json_response({"Message":"No file selected for uploading"},400)
        elif file and allowed_file(file.filename):

            # filename = secure_filename(file.filename)
            filename = file.filename
            filename = now+filename
            filename2 = secure_filename(filename)
            if filename.lower().endswith('.png'):

                file.save(os.path.join(UPLOAD_FOLDER, filename2))

                sess = db.session
                insertdetial = img_data(img_name=filename2)
                sess.add(insertdetial)
                sess.commit()
                return json_response({"filename":filename2 }, 200)

            elif filename.lower().endswith(('.jpg')):
                newfilename = filename.replace('.jpg', '.png')
                file.save(os.path.join(UPLOAD_FOLDER, now+newfilename))

                sess = db.session
                insertdetial = img_data(img_name=now+newfilename)
                sess.add(insertdetial)
                sess.commit()

                return json_response({"filename":now+newfilename},200)

            elif filename.lower().endswith(('.pdf')):
                file.save(os.path.join(UPLOAD_FOLDER_PDF, now+filename))
                pathsend = UPLOAD_FOLDER_PDF + now+filename
                print("pathsend ", pathsend)
                print("save pdf :", os.path.join(UPLOAD_FOLDER_PDF, now+filename))
                json_filename = convert2png(UPLOAD_FOLDER_PDF, now+filename)

                return json_response({"filename": json_filename})
        else:
            print ("else")
            return json_response({"Message":"This file not support"},400)


#  database
#     from ..database import *
    # upload_position = img_data(img_name=insertpost2,img_position=insertpost)
    # sess.add(upload_position)
    # sess.commit()
    #
    # queryfilename = img_data.query.filter_by(img_name=insertpost2).first()
    # print ("queryfilename ",queryfilename.img_name  , type(queryfilename.img_name))
    # aa = json.loads(queryfilename.img_name)
    # print (aa[0]['filename'] , type(aa))

    # print("getpositon = ",postion)
    # return "abc"

