# -*- coding: utf-8 -*
from PIL import Image, ImageDraw,ImageFont
from  ..database import *
from flask import request , Response , json ,send_file
from wand.image import Image as wi
from ..s3.s3 import *
import os
import cv2
import pytesseract
import json
from apps.app import app ,UPLOAD_FOLDER ,ALLOWED_EXTENSIONS,UPLOAD_FOLDER_PNG
import base64
from ..database import *

# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'  #v4
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'        #v5
font = ImageFont.truetype("THSarabunNew.ttf", 30)


def convert2png(UPLOAD_FOLDER_PDF,filename):
    openpdf = UPLOAD_FOLDER_PDF+filename
    pdf = wi(filename=openpdf, resolution=300)
    pdfimage = pdf.convert("png")
    i = 1
    json_base64 = []
    json_returns3 = []
    return_filename =[]

    print(pdf)
    for img in pdfimage.sequence:
        if filename.lower().endswith(('.pdf')):
            newfilename = filename.replace('.pdf', '')

        pathsaveconvert = UPLOAD_FOLDER + newfilename + "_" + str(i) + ".png"
        filenamesave =  newfilename + "_" + str(i) + ".png"
        page = wi(image=img)
        page.save(filename=pathsaveconvert)
        print ("path : " , pathsaveconvert)
        return_filename.append(filenamesave)

        sess = db.session
        insertdetial = img_data(img_name=filenamesave)
        sess.add(insertdetial)
        sess.commit()

        # s3_upload(pathsaveconvert, path_png, filenamesave)
        # link = s3_generatelink(path_png, filenamesave)
        # json_returns3.append({"filename":filenamesave ,"link_s3":link })


        # with open("{}".format(pathsaveconvert), "rb") as img_file:
        #     base64encode = base64.b64encode(img_file.read())
        #     aaa = str(base64encode).replace("b'", "data:image/png;base64,")
        json_base64.append(filenamesave)
        i += 1
    os.remove(openpdf)
    print (json_base64)
    return json_base64

def plot_image(postion):

    for i in range(len(postion)):
        print ("i in plot ", i+1)
        filename = (postion[i]['filename'])
        openpng = UPLOAD_FOLDER + filename
        print ("filename in plot ",filename)
        print("filename in plot type ", type(filename))
        print("openpng in plot ", openpng)
        im = Image.open(r"{}".format(openpng))
        draw = ImageDraw.Draw(im)
        x1 = (postion[i]['position']['x1'])
        x2 = (postion[i]['position']['x2'])
        y1 = (postion[i]['position']['y1'])
        y2 = (postion[i]['position']['y2'])
        label = (postion[i]['label'])
        draw.rectangle(((x1, y1),
                        (x2, y2)),
                       outline='red', width=3)

        # draw.rectangle(((x1, y1 -20), (x2, y1 + 10)), fill="black", width=10)
        # draw.text((x1, y1 -17), label, font=font, fill="white")
        print("123132132",filename)
        savefilename_plot = filename.replace('.png','')
        print (savefilename_plot)
        im.save(os.path.join(UPLOAD_FOLDER, "{}.png".format(savefilename_plot)))

    # os.remove(openpng)

def image2string(postion):
    jstr = []
    jsimage = []

    for i in range(len(postion)):
        filename = postion[i]['filename']
        opontoai = UPLOAD_FOLDER + filename

        img = cv2.imread(opontoai , 0)

        x1 = (postion[i]['position']['x1'])
        x2 = (postion[i]['position']['x2'])
        y1 = (postion[i]['position']['y1'])
        y2 = (postion[i]['position']['y2'])
        label = (postion[i]['label'])
        print ("xy post","x1:",x1 ,"y1:",y1,"x2:",x2,"y2:",y2)
        print ("img = " ,img)
        outcome = img[y1:y2, x1:x2]
        output = pytesseract.image_to_string((outcome), lang='tha+eng', config='--psm 6')
        jstr.append({"filename": filename, "label": label, "output": output})

    plot_image(postion)

    savefilename_plot = filename.replace('.png', '')
    save_s3 = os.path.join(UPLOAD_FOLDER, "{}.png".format(savefilename_plot))
    s3_upload(save_s3, path_png, "{}.png".format(savefilename_plot))
    link = s3_generatelink(path_png,"{}.png".format(savefilename_plot))
    jsimage.append({"image_plot": link})
    json_return = {"result": jstr,"image":jsimage}

    # print("json_return :", json_return)

    return json_response({"msg": json_return})



def json_response(messages=None, status=None, headers=None):
    if headers == None:
        headers = dict()
    headers.update({"Content-Type": "application/json"})
    contents = json.dumps(messages).replace('\\\"', '')
    if(status == None):
        status = 200
    resp = Response(response=contents, headers=headers, status=int(status))
    return resp