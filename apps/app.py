# -*- coding: utf-8 -*
print ("..init apps")
from flask import Flask, render_template
import os
import sys
UPLOAD_FOLDER = 'D:/project_ocr/image_png/'
UPLOAD_FOLDER_PDF = 'D:/project_ocr/pdf/'
UPLOAD_FOLDER_PNG = 'D:/project_ocr/image_plot_png/'
ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg'])



argv = sys.argv
if len(argv) > 1:
    if argv[1] == '--prod':
        UPLOAD_FOLDER = '/data/ocr/image/'
        UPLOAD_FOLDER_PDF = '/data/ocr/pdf/'

app = Flask(__name__)
# CORS(app)
#         app.config.from_object('config')
#         app.secret_key = 'super secret key'
#         if not os.path.exists(os.getcwd()+dir_name):
#             os.mkdir(os.getcwd()+dir_name)
#             print("Directory ", dir_name,  " Created ")
#         else:
#             print("Directory ", dir_name,  " already exists")

# @app.after_request
# def after_request(response):
#     response.headers.add(
#         'Access-Control-Allow-Origin',
#         'http://localhost:4200'
#     )
#     response.headers.add(
#         'Access-Control-Allow-Headers',
#         'Content-Type,Authorization'
#     )
#     response.headers.add(
#         'Access-Control-Allow-Methods',
#         'GET,PUT,POST,DELETE,OPTIONS'
#     )
#     return response

@app.after_request
def after_request(response):
    response.headers.add(
        'Access-Control-Allow-Origin',
        '*'
        # 'http://localhost:4200'

    )
    response.headers.add(
        'Access-Control-Allow-Credentials',
        'true'
    )
    response.headers.add(
        'Access-Control-Allow-Headers',
        'X-Requested-With,Content-type,withCredentials,authorization'
    )
    response.headers.add(
        'Access-Control-Allow-Methods',
        'GET,PUT,POST,DELETE,OPTIONS'
    )

    return response
print ('..finish init apps')
